<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 ********************************************************************
 * @category   BelVG
 * @package    BelVG_ImportAdvance
 * @copyright  Copyright (c) BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */

namespace BelVG\ImportAdvance\Plugin\ConfigurableImportExport\Product;

class Validator
{
    /**
     * @var \BelVG\ImportAdvance\Helper\Attributes
     */
    protected $helper;

    /**
     * Validator constructor.
     *
     * @param \BelVG\ImportAdvance\Helper\Attributes $helper
     */
    public function __construct(\BelVG\ImportAdvance\Helper\Attributes $helper)
    {
        $this->helper = $helper;
    }

    /**
     * @param \Magento\ConfigurableImportExport\Model\Import\Product\Type\Configurable $subject
     * @param array                                                                    $rowData
     * @param                                                                          $rowNum
     * @param bool                                                                     $isNewProduct
     */
    public function beforeIsRowValid(
        \Magento\ConfigurableImportExport\Model\Import\Product\Type\Configurable $subject,
        array $rowData, $rowNum, $isNewProduct = true
    ) {
        $options = $this->helper->getNewOptions();
        foreach ($options as $_option) {
            $subject->addSuperOption($_option['code'], $_option['value'], $_option['attr_code_id']);
        }
    }
}