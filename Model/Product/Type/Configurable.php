<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 ********************************************************************
 * @category   BelVG
 * @package    BelVG_ImportAdvance
 * @copyright  Copyright (c) BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */

namespace BelVG\ImportAdvance\Model\Product\Type;

use Magento\ConfigurableImportExport\Model\Import\Product\Type\Configurable as ConfigurableOrig;

class Configurable extends ConfigurableOrig
{
    /**
     * @param $superAttrCode
     * @param $optionKey
     * @param $optionValue
     *
     * @return $this
     */
    public function addSuperOption($superAttrCode, $optionKey, $optionValue)
    {
        if (isset($this->_superAttributes[$superAttrCode])) {
            $this->_superAttributes[$superAttrCode]['options'][$optionKey] = $optionValue;
        }
        return $this;
    }
}