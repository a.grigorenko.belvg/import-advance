#Import Advance
##Magento 2
This module allows automatically create new options for attributes with select, multi-select types. Also, this module allows import empty values to reset current product attribute value.

####Tested on Magento 2.1.6 - 2.2.5