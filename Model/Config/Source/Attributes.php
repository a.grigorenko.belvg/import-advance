<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 ********************************************************************
 * @category   BelVG
 * @package    BelVG_ImportAdvance
 * @copyright  Copyright (c) BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */

namespace BelVG\ImportAdvance\Model\Config\Source;

class Attributes implements \Magento\Framework\Option\ArrayInterface
{
	const PRODUCT_TYPE_ID = 4;

    /**
     * Attributes constructor.
     *
     * @param \Magento\Catalog\Model\ResourceModel\Eav\Attribute $attributeFactory
     */
	public function __construct(
        \Magento\Catalog\Model\ResourceModel\Eav\Attribute $attributeFactory
    ) {
        $this->_attributeFactory = $attributeFactory;
	}

    /**
     * @return array
     */
	public function toOptionArray()
	{
		$attributeCollection = $this->_attributeFactory->getCollection();
		$attributeCollection->addFieldToFilter('entity_type_id',['eq' => self::PRODUCT_TYPE_ID]);
		$attributeCollection->setOrder('attribute_id');
		$options= [];
		foreach ($attributeCollection as $attribute) {
			$code = $attribute->getAttributeCode();
			$options[] = ['value' => $code, 'label' => $code];
		}

		return $options;
	}
}