<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 ********************************************************************
 * @category   BelVG
 * @package    BelVG_ImportAdvance
 * @copyright  Copyright (c) BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */

namespace BelVG\ImportAdvance\Plugin\Widget;

class Form
{
    const FIELDSET_ID = 'basic_behavior_fieldset';

    /**
     * @var \BelVG\ImportAdvance\Helper\Attributes
     */
    protected $helper;

    /**
     * Type constructor.
     *
     * @param \BelVG\ImportAdvance\Helper\Attributes $helper
     * @param \BelVG\ImportAdvance\Helper\Data       $dataHelper
     */
    public function __construct(
        \BelVG\ImportAdvance\Helper\Attributes $helper
    )
    {
        $this->helper = $helper;
    }

    /**
     * @param \Magento\CatalogImportExport\Model\Import\Product\Type\AbstractType $subject
     * @param array                                                               $rowData
     * @param                                                                     $withDefaultValue
     */
    public function beforeSetForm(
        \Magento\Backend\Block\Widget\Form $subject,
        \Magento\Framework\Data\Form $form
    ) {
        ;
        if ($subject instanceof \Magento\ImportExport\Block\Adminhtml\Import\Edit\Form) {

            $fieldSet = $form->getElement(self::FIELDSET_ID);
            $fieldSet->addField(
                self::FIELDSET_ID . '_autocreate_options',
                'checkbox',
                [
                    'name' => \BelVG\ImportAdvance\Helper\Attributes::IMPORT_OPTION_NAME,
                    'label' => __('Auto Create Select/Multiselect options'),
                    'title' => __('Auto Create Select/Multiselect options'),
                    'value' => 1,
                    'checked' => true,
                ]
            );
        }

        return [$form];
    }

}