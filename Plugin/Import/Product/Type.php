<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 ********************************************************************
 * @category   BelVG
 * @package    BelVG_ImportAdvance
 * @copyright  Copyright (c) BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */

namespace BelVG\ImportAdvance\Plugin\Import\Product;

class Type
{
    const CLEAR_PARAM = '__CLEAR__';

    /**
     * @var \BelVG\ImportAdvance\Helper\Attributes
     */
    protected $helper;

    /**
     * Type constructor.
     *
     * @param \BelVG\ImportAdvance\Helper\Attributes $helper
     * @param \BelVG\ImportAdvance\Helper\Data       $dataHelper
     */
    public function __construct(
    	\BelVG\ImportAdvance\Helper\Attributes $helper,
    	\BelVG\ImportAdvance\Helper\Data $dataHelper
    )
    {
        $this->helper = $helper;
        $this->dataHelper = $dataHelper;
        $this->monitoredAttributes = $dataHelper->getMonitorAttributes();
    }

    /**
     * @param \Magento\CatalogImportExport\Model\Import\Product\Type\AbstractType $subject
     * @param array                                                               $rowData
     * @param                                                                     $withDefaultValue
     */
    public function beforePrepareAttributesWithDefaultValueForSave(
        \Magento\CatalogImportExport\Model\Import\Product\Type\AbstractType $subject,
        array $rowData, $withDefaultValue
    ) {
        $options = $this->helper->getNewOptions();
        foreach($options as $_option) {
            $subject->addAttributeOption($_option['code'], $_option['value'], $_option['attr_code_id']);
        }
    }

    /**
     * @param \Magento\CatalogImportExport\Model\Import\Product\Type\AbstractType $subject
     * @param callable                                                            $proceed
     * @param array                                                               $rowData
     *
     * @return array
     */
    public function aroundClearEmptyData(
    	\Magento\CatalogImportExport\Model\Import\Product\Type\AbstractType $subject,
	    callable $proceed, array $rowData
    ) {
	    $emptyValues = [];
    	foreach ($rowData as $attrCode => $_value ) {
		    if (in_array($attrCode, $this->monitoredAttributes) && (is_null($_value) || $_value =='') ) {
			    $emptyValues[$attrCode] = '';
		    }
	    }

	    $procRowData = $proceed($rowData);

	    if ($emptyValues) {
		    $procRowData = array_merge($procRowData, $emptyValues );
	    }
	    return $procRowData;
    }
}