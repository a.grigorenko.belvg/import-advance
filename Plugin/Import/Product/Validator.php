<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 ********************************************************************
 * @category   BelVG
 * @package    BelVG_ImportAdvance
 * @copyright  Copyright (c) BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */

namespace BelVG\ImportAdvance\Plugin\Import\Product;

use Magento\CatalogImportExport\Model\Import\Product;

class Validator
{
    /**
     * @var \BelVG\ImportAdvance\Helper\Attributes
     */
    protected $helper;

    /**
     * @var Product
     */
    protected $product;

    /**
     * Validator constructor.
     *
     * @param \BelVG\ImportAdvance\Helper\Attributes      $helper
     * @param Product                                     $importProduct
     * @param \Magento\Framework\Filesystem\DirectoryList $dir
     * @param \Magento\Framework\File\Csv                 $csv
     * @param \Psr\Log\LoggerInterface                    $logger
     */
    public function __construct(
        \BelVG\ImportAdvance\Helper\Attributes $helper,
        \Magento\CatalogImportExport\Model\Import\Product $importProduct,
        \Magento\Framework\Filesystem\DirectoryList $dir,
        \Magento\Framework\File\Csv $csv,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->logger = $logger;
        $this->helper = $helper;
        $this->product = $importProduct;
    }

    /**
     * @param Product\Validator $subject
     * @param                   $attrCode
     * @param array             $attrParams
     * @param array             $rowData
     *
     * @return array
     */
    public function beforeIsAttributeValid(
        \Magento\CatalogImportExport\Model\Import\Product\Validator $subject,
        $attrCode,
        array $attrParams,
        array $rowData
    ) {
        if (!$this->helper->isEnabledToAutoCreate()) {
            return array($attrCode, $attrParams, $rowData);
        }
        try {
            switch ($attrParams['type']) {
                case 'select':
                case 'multiselect':
                    $values = explode(Product::PSEUDO_MULTI_LINE_SEPARATOR, $rowData[$attrCode]);
                    $valid = true;
                    $addedNew = false;
                    foreach ($values as $value) {
                        $valid = $valid && isset($attrParams['options'][$value]);

                        if (!$valid && !empty($value) && $value) {
                            $attrCodeId = $this->helper->createOrGetId($attrCode, $value);
                            if ($attrCodeId) {
                                $addedNew = true;
                                $attrParams['options'][strtolower($value)] = $attrCodeId;
                                $this->helper->rememberNewOption($attrCode, $value, $attrCodeId);
                            }
                        }
                    }
                    if ($addedNew) {
                        return array($attrCode, $attrParams, $rowData);
                    }
                    break;
            }
        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
        }
    }
}